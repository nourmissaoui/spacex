package com.example.spacex

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//creating the database logic, extending the SQLiteOpenHelper base class
class SqlDBHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "SpaceX"
        private val TABLE_LUNCH = "lunch"
        private val TABLE_ROCKET = "ROCKET"
        private val FLIGHTNUMBER = "flightNumber"
        private val MISSON_NAME = "missionName"
        private val LUNCH_YEAR = "launchYear"
        private val ROCKET_ID = "rocketId"
        private val LAUNCH_SUCESS = " launchSuccess"
        private val DETAILS = "details"
        private val LAUNCH_DATE_UTC = "launchDateUTC"
        private val ROCKET_NAME = "rocketname"
        private val ROCKET_TYPE = "rockettype"


    }

    override fun onCreate(db: SQLiteDatabase?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        create_tables(db)
        insert()
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_LUNCH + ", " + TABLE_ROCKET,)
        onCreate(db)
    }
    private fun create_tables(db: SQLiteDatabase?){
        val CREATE_LUNCH_TABLE = ("CREATE TABLE" + TABLE_LUNCH + " (" +
                FLIGHTNUMBER + " INTEGER NOT NULL, " +
                MISSON_NAME + " TEXT NOT NULL, " +
                LUNCH_YEAR + " INTEGER AS Int NOT NULL DEFAULT 0, " +
                ROCKET_ID + " TEXT NOT NULL, " +
                DETAILS + " TEXT, " +
                LAUNCH_SUCESS + " INTEGER AS Boolean DEFAULT NULL," +
                LAUNCH_DATE_UTC + " TEXT NOT NULL,"

                )
        val CREATE_ROCKET_TABLE = ("CREATE TABLE" + TABLE_ROCKET + " (" +
                ROCKET_ID + "TEXT NOT NULL PRIMARY KEY," +
                ROCKET_NAME + "TEXT NOT NULL," +
                ROCKET_TYPE + "TEXT NOT NULL)")
        db?.execSQL(CREATE_LUNCH_TABLE)
        db?.execSQL(CREATE_ROCKET_TABLE)

    }

    private fun insert() {
        val db = this.writableDatabase
        val retfrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.spacexdata.com/v3/")
            .build()
        val jsonapi = retfrofit.create(jsonAPI::class.java)
        val rocketcall: Call<List<Rocket>> = jsonapi.getInforockets()
        val lunchcall:  Call<List<RocketLaunch>> = jsonapi.getInfoLunch()
        rocketcall.enqueue(object : Callback<List<Rocket>> {
            override fun onFailure(call: Call<List<Rocket>>, t: Throwable) {
                Log.e("Error", t.message.toString())
            }


            override fun onResponse(call: Call<List<Rocket>>, response: Response<List<Rocket>>) {
                val mmodel: List<Rocket> = response.body()!!
                Log.e("tag", "onResponse: ${mmodel.size}")

                for (i in mmodel) {

                    var x = Rocket(
                        i.id,
                        i.name,
                        i.type

                    )

                    val contentValues = ContentValues()
                    contentValues.put(ROCKET_ID, x.id )
                    contentValues.put(ROCKET_NAME, x.name) // EmpModelClass Name
                    contentValues.put(ROCKET_TYPE, x.type) // EmpModelClass Phone
                    // Inserting Row
                     db.insert(TABLE_ROCKET, null, contentValues)
                    //2nd argument is String containing nullColumnHack
                     db.close() // Closing database connection





                }

            }
        }
        )
       lunchcall.enqueue(object : Callback<List<RocketLaunch>> {
            override fun onFailure(call: Call<List<RocketLaunch>>, t: Throwable) {
                Log.e("Error", t.message.toString())
            }


            override fun onResponse(call: Call<List<RocketLaunch>>, response: Response<List<RocketLaunch>>) {
                val mmodell: List<RocketLaunch> = response.body()!!
                Log.e("tag", "onResponse: ${mmodell.size}")

                for (i in mmodell) {

                    var x = RocketLaunch(
                        i.flightNumber,
                        i.missionName,
                        i.launchYear,
                        i.launchDateUTC,
                        i.rocket,
                        i.details,
                        i.launchSuccess
                      )

                    val contentValues = ContentValues()
                    contentValues.put(FLIGHTNUMBER, x.flightNumber )
                    contentValues.put(MISSON_NAME, x.missionName) // EmpModelClass Name
                    contentValues.put(DETAILS, x.details) // EmpModelClass Phone
                    contentValues.put(LAUNCH_DATE_UTC, x.launchDateUTC)
                    contentValues.put(LAUNCH_SUCESS,x.launchSuccess)
                    contentValues.put(ROCKET_ID,x.rocket.id)
                    contentValues.put(LUNCH_YEAR, x.launchYear)

                    // Inserting Row
                    db.insert(TABLE_ROCKET, null, contentValues)
                    //2nd argument is String containing nullColumnHack
                    db.close() // Closing database connection


                }

            }
        }
        )
    }

}