package com.example.spacex

import retrofit2.Call
import retrofit2.http.GET

interface jsonAPI {

    @GET("launches")
    fun getInfoLunch(): Call<List<RocketLaunch>>

    @GET("rockets")
    fun getInforockets(): Call<List<Rocket>>

}